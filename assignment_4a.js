standard_input=process.stdin;
standard_input.setEncoding('utf-8');
console.log('Enter the number');
standard_input.on('data',function(data){
    standard_input.pause();
    while(data>0){
        fact=longnumberstring(factorial(data));
        console.log('Factorial is '+fact);
        data=0;
    }
});
function factorial(data){
    if(data==1)
        return 1;
    else
        return (data*factorial(data-1));
}


function longnumberstring(n){
    var str, str2= '', data= n.toExponential().replace('.','').split(/e/i);
    str= data[0], mag= Number(data[1]);
    if(mag>=0 && str.length> mag){
        mag+=1;
        return str.substring(0, mag)+'.'+str.substring(mag);            
    }
    if(mag<0){
        while(++mag) str2+= '0';
        return '0.'+str2+str;
    }
    mag= (mag-str.length)+1;
    while(mag> str2.length){
        str2+= '0';
    }
    return str+str2;
}
