var standard_input=process.stdin;
console.log("First 100 prime numbers");
for(var count=2;count<=100;count++){
    var notPrime = false;
    var i=2;
    while(i<=count && notPrime!=true){
        if(count%i==0 && i!==count){    
            notPrime = true;
        }
        i+=1;
    }
    if(notPrime==false){
        console.log(count)
    }
}
