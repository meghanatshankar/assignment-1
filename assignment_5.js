var standard_input = process.stdin;
var fact=1,count=0;
console.log('Enter the data');
standard_input.setEncoding('utf-8');
standard_input.on('data',function(data){
    standard_input.pause();
    for(var i=1;i<=data;i++){
        fact=fact*i;
    }
    console.log('Factorial is '+longnumberstring(fact));
    var len=longnumberstring(fact).toString().length;
    var digits = longnumberstring(fact).toString().split('');
    for(var i=len-1; i >= 0; i--){
        if(digits[i]=='0'){
            count+=1;
        }
        else{
            break;
        }
    }
    console.log("Trailing number of zeroes ", count);
});

function longnumberstring(n){
    var str, str2= '', data= n.toExponential().replace('.','').split(/e/i);
    str= data[0], mag= Number(data[1]);
    if(mag>=0 && str.length> mag){
        mag+=1;
        return str.substring(0, mag)+'.'+str.substring(mag);            
    }
    if(mag<0){
        while(++mag) str2+= '0';
        return '0.'+str2+str;
    }
    mag= (mag-str.length)+1;
    while(mag> str2.length){
        str2+= '0';
    }
    return str+str2;
}
