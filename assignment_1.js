var standard_input = process.stdin;
console.log("Enter number")
standard_input.on('data',function(data){
    standard_input.pause();
    var rem=0;
    var ans=0;
    standard_input.setEncoding('utf-8');
    temp=data;
    while(data!=0){   
        rem=data%10;
        ans=ans*10+rem;
        data=parseInt(data/10);
    }
    if(temp==ans){
        console.log("It is a palindrome number");
    }
    else{
        console.log("It is not a palindrome number");
    }
});